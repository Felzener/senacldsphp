<?php
echo "<pre>";

//constantes no PHP 

define ('QTD_PAGINAS', 10);

echo "valor da minha constante é " . QTD_PAGINAS;

// variavel para passar valor para uma constante 
//$qtd_paginas = 25;

$ip_do_banco = '192.168.45.12';

define('IP_DO_BANCO', $ip_do_banco);

echo "\no ip do SGDB é" . IP_DO_BANCO;

//  Constantes Magicas 
echo "\nestou na linha : " . __LINE__;
echo "\nestou na linha : " . __LINE__;
echo "\neste é o arquivo : " . __FILE__ ;

//Muito bom para depurar o codigo 
echo "\n\n";
var_dump ($ip_do_banco);


/*
*
*
*Agora fica mais legal 
*é a hora dos vetores 
* ARRAY
*
*
*/
echo "\n\n";
$dias_da_semana = ['dom',
                    'seg',
                    'ter',
                    'qua',
                    'qui',
                    'sex',
                    'sab',
                    ];

unset ($dias_da_semana); // destroi a variavel (apaga total!!!)

$dias_da_semana [0] = 'dom';
$dias_da_semana [1] = 'seg';
$dias_da_semana [2] = 'ter';
$dias_da_semana [3] = 'qua';
$dias_da_semana [4] = 'qui';
$dias_da_semana [5] = 'sex';
$dias_da_semana [6] = 'sab';

var_dump ($dias_da_semana);

unset ($dias_da_semana);

$dias_da_semana = array (0 => 'dom',
                         1 => 'seg',
                         2 => 'ter',
                         3 => 'qua',
                         4 => 'qui',
                         5 => 'sex',
                         6 => 'sab',
                        );








            echo "\n\n";
            echo "</pre>";


var_dump ($dias_da_semana);