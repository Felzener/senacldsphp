
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/04d8502c9f.js"></script>
    <title>Listar Usuário</title>
</head>

<?php

session_start();

// verifica se o usuario pode acessar as funcionalidades

if ( !isset($_SESSION['id_usuario']) ) {
    header ('Location: ../login.php');
    exit();
}
?>


<body>

<h3>Listar Usuários</h3>
    <div class='container'>
        <table class="table table-sm table-dark">
            <thead>
              <tr>
                <th scope="col">#id_usuário</th>
                <th scope="col">Nome</th>
                <th scope="col">Email</th>
                <th scope="col">Ação</th>
              </tr>
            </thead>

            <?php

                // incluindo conexao com DB
                      
                if ( $db = mysqli_connect('localhost', 'root', '','aula_php', 3307)){
                    echo " CONECTADO NO BANCO DE DADOS <br>";
          }
           else {
                  
                   echo "Problema ao conectar ao SGDB";
          }
                    //echo " CONECTADO NO BANCO DE DADOS <br>";
            
                                
                $sql = "SELECT id,nome,email FROM tb_usuario;";
                $busca = mysqli_query($db, $sql);

                    while ($array = mysqli_fetch_array($busca)){
                            $id = $array ['id'];
                            $nome = $array ['nome'];
                            $email = $array ['email'];

            ?>

             <tr>
                  <td><?php echo $id ?></td>
                  <td><?php echo $nome ?></td>
                  <td><?php echo $email ?></td>
                  <td><a class="btn btn-success btn-sm" style="color:#ffffff" href="_editar_usuario.php?id=<?php echo $id?>" 
                    role="button"><i class="far fa-thumbs-up"></i>&nbsp; &nbsp; Editar</a>
                    <a class="btn btn-danger btn-sm" style="color:#ffffff" href="_deletar_usuario.php?id=<?php echo $id?>" 
                    role="button"><i class="far fa-thumbs-up"></i>&nbsp; &nbsp; deletar</a></td>
             </tr>
            <?php } ?>
    </div>
            


<?php

echo '<a href="../menu.php">voltar</a><br>';

?>
            
            

</body>
</html>




