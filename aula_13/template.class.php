<?php

class template {
    private $arquivo;
    private $variaveis;

    public function __construct (string $arquivo){
        $this->arquivo = $arquivo;
    }

    public function SetVariaveis (array $vetor): bool {
        $this->variaveis = $vetor;
    }

    public function mostrar() {
        include ( $this->arquivo);
    }
}

