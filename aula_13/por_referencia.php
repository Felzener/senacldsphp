<?php

$v = 10; 
// com o & seleciona a variavel por referencia alterando todas que estao fora da funcao.... 
// nao recomendaver aq nao ser que tenha uma boa razao para usar.
function muda (int &$x) : bool {
    $x = 20; 
    return true;

}

echo '<pre> V Vale : ' . $v;
muda ($v);
echo"\nV vale : $v";