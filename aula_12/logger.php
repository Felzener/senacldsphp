<?php 

/* função para gravar logs do sistema  */
/*
function logger (string $str):bool{
    $fp = fopen ('log.txt', 'a');
    if (fwrite($fp, $str) ) {
        fclose ($fp);
        return true;
    } else {
        fclose($fp);
        return false;
    }
}
*/

/* exemplo mais de um parametro */
/*
function logger (string $str, int $nr_linha):bool{
    $fp = fopen ('log.txt', 'a');
    if (fwrite($fp, $nr_linha . ' : ' .  $str) ) {
        fclose ($fp);
        return true;
    } else {
        fclose($fp);
        return false;
    }
}
*/


/* exemplo mais de um parametro sendo nao obrigatorio [] */
/*
function logger (string $str, int $nr_linha = null):bool{
    $fp = fopen ('log.txt', 'a');
    if (fwrite($fp, $nr_linha . ' : ' .  $str) ) {
        fclose ($fp);
        return true;
    } else {
        fclose($fp);
        return false;
    }
}
*/