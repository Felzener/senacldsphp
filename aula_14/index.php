<?php
// preparando conexao com banco de dados usando PDO 
echo "<pre>";
// definindo variável id .
$id = 3;

$dsn = 'mysql:dbname=aula_php;host=127.0.0.1:3307';
$user = 'root';
$password = '';


$objPDO = new PDO($dsn, $user, $password);

var_dump ($objPDO);
// fazendo quebra de linha (pulando uma linha)
echo "\n\n";

$objStmt = $objPDO->prepare('SELECT nome, email FROM tb_usuario WHERE id = :id');

var_dump ($objStmt);


$objStmt -> execute(array (':id' => $id));

$result = $objStmt -> fetch(PDO::FETCH_ASSOC);
echo "\n\n";

var_dump ($result);







